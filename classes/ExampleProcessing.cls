/**
 * @File Name          : ExampleProcessing.cls
 * @Description        :
 * @Author             : wbatista@moldsoft.com.br
 * @Group              :
 * @Last Modified By   : wbatista@moldsoft.com.br
 * @Last Modified On   : 25/10/2019 14:39:28
 * @Modification Log   :
 * Ver       Date            Author                  Modification
 * 1.0    25/10/2019   wbatista@moldsoft.com.br     Initial Version
 **/
public with sharing class ExampleProcessing implements InterfaceProcessingQueue {
	/**
	 * [processingQueue processa a fila e insere a coligada]
	 * @param queueId   [Id da fila]
	 * @param eventName [Nome do evento]
	 * @param payload   [Corpo JSON]
	 */
	public static void processingQueue(String queueId, String eventName, String payload) {
		//Aqui vocè faz a chamada de webservice assíncrona, pode ser Batch, Queue ou Future
		//Lembrar de quando for testar Test.startTest(); e definir um Mock
		makeCallout(queueId, eventName, payload);

	}

	@Future(Callout = true)
	public static void makeCallout(String queueId, String eventName, String payload) {

		//Colocar o endpoint em Configuração personalizada ou Metadado personalizado
		String VIACEP_ENDPOINT = 'https://viacep.com.br/ws/' + payload + '/json/';
		String CONTENT_TYPE = 'application/json';
		Integer TIMEOUT = 120000;

		try {

			//Não esquecer de cadastrar o viacep como site remoto
			HttpRequest req = new HttpRequest();
			req.setEndpoint(VIACEP_ENDPOINT);
			req.setMethod('GET');
			req.setHeader('Content-Type', CONTENT_TYPE);
			req.setTimeout(TIMEOUT);

			Http h = new Http();
			HttpResponse res = h.send(req);
			system.debug(res.getBody());
			if (res.getStatusCode() == 200) {
				
				//fazer alguma tratativa

				QueueBO.getInstance().updateQueueWithResponse(queueId, '', res.getBody(), String.valueOf(res.getStatusCode()));
				
			}else{
				String exceptionMessage = 'VIACEP getStatusCode / ' + res.getStatusCode() + ' / ' + res.getStatus();
				system.debug(exceptionMessage);
				QueueBO.getInstance().updateQueueWithResponse(queueId, exceptionMessage, res.getBody(), String.valueOf(res.getStatusCode()));
			}

		} catch (Exception ex) {
			String exceptionMessage = 'Exception / ' + ex.getMessage() + ' / ' + ex.getStackTraceString();
			system.debug(exceptionMessage);
			QueueBO.getInstance().updateQueue(queueId, exceptionMessage);
		}
		
	}

}