/**
 * @File Name          : ExampleProcessingTest.cls
 * @Description        :
 * @Author             : wbatista@moldsoft.com.br
 * @Group              :
 * @Last Modified By   : wbatista@moldsoft.com.br
 * @Last Modified On   : 25/10/2019 15:57:37
 * @Modification Log   :
 * Ver       Date            Author                  Modification
 * 1.0    25/10/2019   wbatista@moldsoft.com.br     Initial Version
 **/
@ IsTest
private with sharing class ExampleProcessingTest {

	private static string EVENT_NAME_CEP;
	private static String CEP = '60140140';

	static {
		EVENT_NAME_CEP = QueueBO.getInstance().events [0].name();

	}

	@IsTest
	static void insertTest() {
		Test.setMock(HttpCalloutMock.class, new MockExampleProcessing());
		Test.startTest();
		QueueBO.getInstance().createQueue(EVENT_NAME_CEP, CEP);
		Test.stopTest();
	}

    @IsTest
	static void testErrorStatusCode() {
		Test.setMock(HttpCalloutMock.class, new MockExampleProcessing(500));
		Test.startTest();
		QueueBO.getInstance().createQueue(EVENT_NAME_CEP, CEP);
		Test.stopTest();
	}
    

	@IsTest
	static void deleteTest() {
		Test.setMock(HttpCalloutMock.class, new MockExampleProcessing());

		Test.startTest();
		String queueId = QueueBO.getInstance().createQueue(EVENT_NAME_CEP, CEP);
		Test.stopTest();
		Queue__c q = new Queue__c(Id = queueId);
		QueueDAO.getInstance().deleteData(q);

	}

    @IsTest
    static void methodName(){
        Test.setMock(HttpCalloutMock.class, new MockExampleProcessing());
        QueueBO.getInstance().createQueueBySchedule(EVENT_NAME_CEP, CEP, Test.getStandardPricebookId());
		QueueBO.getInstance().createQueueWithRegister(EVENT_NAME_CEP, CEP, Test.getStandardPricebookId());
        String queueId = QueueBO.getInstance().createQueue(EVENT_NAME_CEP, CEP);
        Test.startTest();

        QueueDAO.getInstance().getQueueById(queueId);
        QueueDAO.getInstance().getQueueByEventNameAndStatus(EVENT_NAME_CEP, 'SUCCESS', 50);
        List<Queue__c> queues = QueueDAO.getInstance().getQueueByStatus('SUCCESS', 50);
        List<Queue__c> queusToInsert = new List<Queue__c>();
        
        for(Queue__c q: queues){
            Queue__c que = new Queue__c();
            que.EventName__c = q.EventName__c;
            que.Payload__c = q.Payload__c;
        }

        QueueDAO.getInstance().insertData(queusToInsert);
        QueueDAO.getInstance().updateData(queues);
        QueueDAO.getInstance().deleteData(queues);
    }
}