/**
 * @File Name          : InterfaceProcessingQueue.cls
 * @Description        : 
 * @Author             : wbatista@moldsoft.com.br
 * @Group              : 
 * @Last Modified By   : wbatista@moldsoft.com.br
 * @Last Modified On   : 25/10/2019 11:39:17
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    25/10/2019   wbatista@moldsoft.com.br     Initial Version
**/
public interface InterfaceProcessingQueue {
	void processingQueue( String queueId, String eventName, String payload );
}