/**
 * @File Name          : InterfaceTriggerHandler.cls
 * @Description        : 
 * @Author             : wbatista@moldsoft.com.br
 * @Group              : 
 * @Last Modified By   : wbatista@moldsoft.com.br
 * @Last Modified On   : 25/10/2019 10:45:38
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.1    25/10/2019   wbatista@moldsoft.com.br     Initial Version
**/
public interface InterfaceTriggerHandler {    
    void beforeUpdate();
    
    void beforeInsert();
    
    void beforeDelete();
    
    void afterUpdate();
    
    void afterInsert();
    
    void afterDelete();
}