/**
 * @File Name          : MockExampleProcessing.cls
 * @Description        :
 * @Author             : wbatista@moldsoft.com.br
 * @Group              :
 * @Last Modified By   : wbatista@moldsoft.com.br
 * @Last Modified On   : 25/10/2019 13:57:22
 * @Modification Log   :
 * Ver       Date            Author                  Modification
 * 1.0    25/10/2019   wbatista@moldsoft.com.br     Initial Version
 **/
public with sharing class MockExampleProcessing implements HttpCalloutMock {

	Integer statusCode { get; set; }

	public MockExampleProcessing(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public MockExampleProcessing() {
		this.statusCode = 200;
	}

	public HTTPResponse respond(HTTPRequest req) {

		// Create a fake response
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		res.setBody('   {  ' +
				    '       "cep": "60140-140",  ' +
				    '       "logradouro": "Rua João Carvalho",  ' +
				    '       "complemento": "",  ' +
				    '       "bairro": "Aldeota",  ' +
				    '       "localidade": "Fortaleza",  ' +
				    '       "uf": "CE",  ' +
				    '       "unidade": "",  ' +
				    '       "ibge": "2304400",  ' +
				    '       "gia": ""  ' +
				    '  }  ');
		res.setStatusCode(statusCode);
		return res;
	}

}