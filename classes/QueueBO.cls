/**
 * @File Name          : QueueBO.cls
 * @Description        :
 * @Author             : wbatista@moldsoft.com.br
 * @Group              :
 * @Last Modified By   : wbatista@moldsoft.com.br
 * @Last Modified On   : 25/10/2019 14:45:47
 * @Modification Log   :
 * Ver       Date            Author                  Modification
 * 1.0    25/10/2019   wbatista@moldsoft.com.br     Initial Version
 **/
public with sharing class QueueBO {

	/*
	 Singleton
	 */
	private static final QueueBO instance = new QueueBO();
	public List<QueueEventNames> events;

	private QueueBO() {
		this.events = QueueEventNames.values();
	}

	public enum QueueStatus {
		CREATED, 
		SUCCESS, 
		ERROR
	}

	public enum QueueEventNames {
		VIA_CEP, 
		EXAMPLE_TWO_PROCESSING, 
		EXAMPLE_THREE_PROCESSING
	}

	public static QueueBO getInstance() {
		return instance;
	}

	private static map<String, InterfaceProcessingQueue> mapToExecute;
	static {
		mapToExecute = new map<String, InterfaceProcessingQueue>();
		mapToExecute.put(QueueEventNames.VIA_CEP.name(), new ExampleProcessing());

	}

	/*
	 Cria fila de processamento
	 @param eventName string com o nome do evento a ser processado
	 @param payload string com os JSON do objeto a ser processado
	 */
	public void createQueueWithRegister(String eventName, String payLoad, String recordId) {
		Queue__c queue = new Queue__c();
		queue.EventName__c = eventName;
		queue.Payload__c = payLoad;
		queue.Status__c = QueueStatus.CREATED.name();
		queue.RecordId__c = recordId;

		QueueDAO.getInstance().insertData(queue);
	}

	public void createQueueBySchedule(String eventName, String payLoad, String recordId) {
		Queue__c queue = new Queue__c();
		queue.EventName__c = eventName;
		queue.Payload__c = payLoad;
		queue.Status__c = QueueStatus.CREATED.name();
		queue.RecordId__c = recordId;
		queue.ProcessBySchedule__c = true;
		QueueDAO.getInstance().insertData(queue);
	}

	public String createQueue(String eventName, String payLoad) {
		Queue__c queue = new Queue__c();
		queue.EventName__c = eventName;
		queue.Payload__c = payLoad;
		queue.Status__c = QueueStatus.CREATED.name();

		QueueDAO.getInstance().insertData(queue);
		return queue.Id;
	}

	/*
	 Atualiza fila de processamento
	 @param queueId string com o id da fila para atualização
	 @param dmlExceptionStackTrace string com possivel erro de processamento da fila
	 */
	public void updateQueue(String queueId, String dmlExceptionStackTrace) {
		Queue__c queue = new Queue__c();
		queue.Id = queueId;
		queue.Status__c = dmlExceptionStackTrace.equals('') ? QueueStatus.SUCCESS.name() :QueueStatus.ERROR.name();
		queue.Exception__c = dmlExceptionStackTrace;
        QueueUtil.increment = true;
		QueueDAO.getInstance().updateData(queue);
	}

	/*
	 Atualiza fila de processamento
	 @param queueId string com o id da fila para atualização
	 @param dmlExceptionStackTrace string com possivel erro de processamento da fila
	 */
	public void updateQueueWithResponse(String queueId, String dmlExceptionStackTrace, String retorno, String statusCode) {
		Queue__c queue = new Queue__c();
		queue.Id = queueId;
		queue.Status__c = dmlExceptionStackTrace.equals('') ? QueueStatus.SUCCESS.name() :QueueStatus.ERROR.name();
		queue.Exception__c = dmlExceptionStackTrace;
		queue.Response__c = retorno;
		queue.StatusCode__c = statusCode;
        QueueUtil.increment = true;
		QueueDAO.getInstance().updateData(queue);
	}

	/*
	 Executa o processamento da fila
	 @param queueToProcessing filas para processamento
	 */
	public void executeProcessingQueue(List<Queue__c> queueToProcessing) {
		for(Queue__c queue :queueToProcessing) {
			mapToExecute.get(queue.EventName__c).processingQueue(queue.Id, queue.EventName__c, queue.Payload__c);
		}
	}

}