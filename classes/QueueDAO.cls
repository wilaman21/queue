/**
 * @File Name          : QueueDAO.cls
 * @Description        :
 * @Author             : wbatista@moldsoft.com.br
 * @Group              :
 * @Last Modified By   : wbatista@moldsoft.com.br
 * @Last Modified On   : 25/10/2019 15:52:16
 * @Modification Log   :
 * Ver       Date            Author                  Modification
 * 1.0    25/10/2019   wbatista@moldsoft.com.br     Initial Version
 **/
public with sharing class QueueDAO extends SObjectDAO {
	/*  
	 Singleton
	 */
	private static final QueueDAO instance = new QueueDAO();
	private QueueDAO() {
		super();
	}

	public static QueueDAO getInstance() {
		return instance;
	}

	/*  
	 Retorna a fila pelo Id
	 @param queueId id da fila para reprocessamento
	 */
	public List<Queue__c> getQueueById(String queueId) {
		return [SELECT
			    Id, 
			    Name, 
			    EventName__c, 
			    Status__c, 
			    Payload__c
			    FROM Queue__c
			    WHERE Id = :queueId];
	}

	/*  
	 Retorna filas por status
	 @param queueStatus Status da fila para filtro
	 */
	public List<Queue__c> getQueueByStatus(String queueStatus, Integer numLimit) {
		return [SELECT
			    Id, 
			    Name, EventName__c, 
			    Status__c, 
			    Payload__c, 
			    CreatedDate
			    FROM Queue__c
			    WHERE Status__c = :queueStatus
			    order by Name desc Limit :numLimit];
	}


	/*  
	 Retorna as filas pelo evento e status
	 @param eventName nome do evento criado
	 @param queueStatus status da fila
	 */
	public List<Queue__c> getQueueByEventNameAndStatus(String eventName, String queueStatus, Integer numLimit) {
		return [SELECT
			    Id, 
			    Name, 
			    EventName__c, 
			    Status__c, 
			    Payload__c, 
			    CreatedDate
			    FROM Queue__c
			    WHERE EventName__c = :eventName
			    AND Status__c = :queueStatus
			    ORDER BY CreatedDate ASC
			    Limit :numLimit];
	}

}