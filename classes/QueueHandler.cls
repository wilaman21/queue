/**
 * @File Name          : QueueHandler.cls
 * @Description        : 
 * @Author             : wbatista@moldsoft.com.br
 * @Group              : 
 * @Last Modified By   : wbatista@moldsoft.com.br
 * @Last Modified On   : 25/10/2019 14:44:43
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    25/10/2019   wbatista@moldsoft.com.br     Initial Version
**/
public with sharing class QueueHandler implements InterfaceTriggerHandler {
    private static final QueueHandler instance = new QueueHandler();
    /** Private constructor to prevent the creation of instances of this class.*/
    private QueueHandler() {}
    
    /**
    * @description Method responsible for providing the instance of this class.
    * @return GeneralSourceTriggerHandler instance.
    **/

    public static QueueHandler getInstance() {
        return instance;
    }    
    
    /**
    * Method to handle trigger before update operations. 
    */
    public void beforeUpdate() {
        QueueUtil.incrementaNumeroVezesProcessado((List<Queue__c>)Trigger.new, (Map<Id,Queue__c>)Trigger.oldMap);
    }
    
    /**
    * Method to handle trigger before insert operations. 
    */ 
    public void beforeInsert() {
    		
    }
    
    /**
    * Method to handle trigger before delete operations. 
    */
    public void beforeDelete() {

    }
    
    /**
    * Method to handle trigger after update operations. 
    */
    public void afterUpdate() {

    }
    
    /**
    * Method to handle trigger after insert operations. 
    */
    public void afterInsert()  {
        system.debug('>>>>');
    	QueueUtil.processaAfterInsert((List<Queue__c>)Trigger.new, (Map<Id,Queue__c>)Trigger.oldMap);
    }
    /**
    * Method to handle trigger after delete operations. 
    */
    public void afterDelete() {

    }

}