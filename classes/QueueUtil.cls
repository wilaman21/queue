/**
 * @File Name          : QueueUtil.cls
 * @Description        : 
 * @Author             : wbatista@moldsoft.com.br
 * @Group              : 
 * @Last Modified By   : wbatista@moldsoft.com.br
 * @Last Modified On   : 25/10/2019 14:44:20
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    25/10/2019   wbatista@moldsoft.com.br     Initial Version
**/
public with sharing class QueueUtil {

    public static Boolean increment = false;

	public static void processaAfterInsert(List < Queue__c > newQueuesList, Map <Id, Queue__c > oldQueuesMap) {
        List<Queue__c> queuesToProcess = new List<Queue__c>();

        for (Queue__c q: newQueuesList) {
            if(q.ProcessBySchedule__c == false){
	               queuesToProcess.add(q);
            }
        }

        if(queuesToProcess.size() > 0){
            QueueBO.getInstance().executeProcessingQueue( queuesToProcess );
        }
    }

    public static void incrementaNumeroVezesProcessado(List < Queue__c > newQueuesList, Map <Id, Queue__c > oldQueuesMap) {

        for (Queue__c q: newQueuesList) {
            if(increment){
	            q.TimesProcessed__c += 1;
            }
        }

    }
}