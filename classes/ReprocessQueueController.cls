/**
 * @File Name          : ReprocessQueueController.cls
 * @Description        :
 * @Author             : wbatista@moldsoft.com.br
 * @Group              :
 * @Last Modified By   : wbatista@moldsoft.com.br
 * @Last Modified On   : 25/10/2019 12:58:01
 * @Modification Log   :
 * Ver       Date            Author                  Modification
 * 1.0    25/10/2019   wbatista@moldsoft.com.br     Initial Version
 **/
public with sharing class ReprocessQueueController {

	private String queueId;

	/*  
	 Construtor
	 */
	public ReprocessQueueController(ApexPages.StandardController stdController) {
		queueId = stdController.getId();
	}

	/*  
	 Metodo principal de execução
	 */
	public void execute() {
		Queue__c queue = QueueDAO.getInstance().getQueueById(queueId) [0];
		reprocessaFila(queue);
	}

	/*  
	 Executa o reprocessamento da fila
	 */
	public static void reprocessaFila(Queue__c queue) {
		QueueBO.getInstance().executeProcessingQueue(new List<Queue__c>{ queue });
	}
}