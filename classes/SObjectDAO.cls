/**
 * @File Name          : SObjectDAO.cls
 * @Description        : Classe que define métodos de operações DML comuns a todos objetos SalesForce.
 Essa classe não pode ser utilizada diretamente, apenas determina o comportamento de suas descendentes.
 * @Author             : wbatista@moldsoft.com.br
 * @Group              :
 * @Last Modified By   : wbatista@moldsoft.com.br
 * @Last Modified On   : 25/10/2019 14:58:12
 * @Modification Log   :
 * Ver       Date            Author                  Modification
 * 1.0    25/10/2019   wbatista@moldsoft.com.br     Initial Version
 **/
public abstract class SObjectDAO {
	/**
	 * Insere um objeto na base de dados.
	 * @param sObj Objeto a ser inserido. Este objeto é qualquer descendente de SObject, sendo possível passar um objeto padrão ou customizado.
	 * @return Um objeto SaveResult indicando o resultado da inserção.
	 */
	public  Database.SaveResult insertData(SObject sObj) {
		Database.SaveResult sr = null;

		try {
			sr = Database.insert(sObj);
		} catch(Exception ex) {
			System.debug('>> EX INSERT: ' + ex.getMessage());
			throw ex;
		}
		return sr;
	}

	/**
	 * Insere uma lista de objetos na base de dados.
	 * @param sObjList A lista de objetos a ser inserida. Esta lista pode ser composta por qualquer
	 * descendente de SObject, sendo possível passar uma lista de objetos padrão ou customizados.
	 * @return Uma lista de objetos SaveResult indicando o resultado da inserção.
	 */
	public  List<Database.SaveResult> insertData(List<SObject> sObjList) {
		List<Database.SaveResult> srList = null;

		try {
			srList = Database.insert(sObjList);
		} catch(Exception ex) {
			System.debug('>> EX INSERT: ' + ex.getMessage());
			throw ex;
		}
		return srList;
	}

	/**
	 * Atualiza um objeto na base de dados.
	 * @param sObj Objeto a ser atualizado. Este objeto é qualquer descendente de SObject, sendo possível passar um objeto padrão ou customizado.
	 * @return Um objeto SaveResult indicando o resultado da atualização.
	 */
	public  Database.SaveResult updateData(SObject sObj) {
		Database.SaveResult sr = null;

		try {
			sr = Database.update(sObj);
		} catch(Exception ex) {
			System.debug('>> EX UPDATE: ' + ex.getMessage());
			throw ex;
		}
		return sr;
	}

	/**
	 * Atualiza uma lista de objetos na base de dados.
	 * @param sObjList A lista de objetos a ser atualizada. Esta lista pode ser composta por qualquer descendente de SObject,
	 * sendo possível passar uma lista de objetos padrão ou customizados.
	 * @return Uma lista de objetos SaveResult indicando o resultado da atualização.
	 */
	public  List<Database.SaveResult> updateData(List<SObject> sObjList) {
		List<Database.SaveResult> srList = null;

		try {
			srList = Database.update(sObjList);
		} catch(Exception ex) {
			System.debug('>> EX UPDATE: ' + ex.getMessage());
			throw ex;
		}
		return srList;
	}

	/**
	 * Deleta um objeto da base de dados.
	 * @sObjList O objeto a ser deletado.
	 * @return Um objeto DeleteResult indicando o resultado da deleção.
	 */
	public  Database.DeleteResult deleteData(SObject sObj) {
		Database.DeleteResult dr = null;

		try {
			dr = Database.delete(sObj);
		} catch(Exception ex) {
			System.debug('>> EX DELETE: ' + ex.getMessage());
			throw ex;
		}
		return dr;
	}

	/**
	 * Deleta uma lista de objetos da base de dados.
	 * @sObjList A lista de objetos a ser deletada. Esta lista pode ser composta por qualquer descendente de SObject
	 * @return Uma lista de objetos DeleteResult indicando o resultado da deleção.
	 */
	public  List<Database.DeleteResult> deleteData(List<SObject> sObjList) {
		List<Database.DeleteResult> drList = null;

		try {
			drList = Database.delete(sObjList);
		} catch(Exception ex) {
			System.debug('>> EX DELETE: ' + ex.getMessage());
			throw ex;
		}
		return drList;
	}
}