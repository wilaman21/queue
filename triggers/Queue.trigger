/**
 * @File Name          : Queue.trigger
 * @Description        : 
 * @Author             : wbatista@moldsoft.com.br
 * @Group              : 
 * @Last Modified By   : wbatista@moldsoft.com.br
 * @Last Modified On   : 25/10/2019 15:39:41
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    25/10/2019   wbatista@moldsoft.com.br     Initial Version
**/
trigger Queue on Queue__c (
    before insert, 
    before update, 
    before delete, 
    after insert, 
    after update, 
    after delete) {
        if (Trigger.isBefore)
            if (Trigger.isUpdate)
                QueueHandler.getInstance().beforeUpdate();
            else if (Trigger.isInsert)
                QueueHandler.getInstance().beforeInsert();
            else
                QueueHandler.getInstance().beforeDelete();
        else
            if (Trigger.isUpdate)
                QueueHandler.getInstance().afterUpdate();
            else if (Trigger.isInsert)
                QueueHandler.getInstance().afterInsert();
            else
                QueueHandler.getInstance().afterDelete();
}